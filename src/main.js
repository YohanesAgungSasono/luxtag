// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import VueJsModal from 'vue-js-modal';
import Notifications from 'vue-notification'

Vue.use(Notifications)
Vue.config.productionTip = false;
Vue.use(VueJsModal, {
  dialog: true,
  dynamic: true,
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
});
